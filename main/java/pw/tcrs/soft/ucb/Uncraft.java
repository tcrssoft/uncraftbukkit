package pw.tcrs.soft.ucb;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Uncraft extends JavaPlugin {
	public PluginManager pm;
	Logger log =Logger.getLogger("Mincraft");

	public void onDisable(){
		log.info("Uncraft Bukkit off");

	}

	@Override
	public void onEnable(){
//		int D = 0;
		log.info("Uncraft Bukkit on");

		//オークのドアから木材を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 6)).addIngredient(Material.WOOD_DOOR));
		//松のドアから木材を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 6, (short)1)).addIngredient(Material.SPRUCE_DOOR_ITEM));
		//白樺のドアから木材を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 6, (short)2)).addIngredient(Material.BIRCH_DOOR_ITEM));
		//ジャングルのドアから木材を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 6, (short)3)).addIngredient(Material.JUNGLE_DOOR_ITEM));
		//アカシアのドアから木材を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 6, (short)4)).addIngredient(Material.ACACIA_DOOR_ITEM));
		//ダークオークのドアから木材を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 6, (short)5)).addIngredient(Material.DARK_OAK_DOOR_ITEM));
		//鉄のドアからインゴットを作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT, 6)).addIngredient(Material.IRON_DOOR));
		//竈から丸石を作成
		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE, 8)).addIngredient(Material.FURNACE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 8)).addIngredient(Material.CHEST));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD, 4)).addIngredient(Material.WORKBENCH));
//		ShapedRecipe LADDERR =new ShapedRecipe(new ItemStack(Material.STICK,7));
//		LADDERR.shape("X X","   ","X").setIngredient('X', Material.LADDER);
//		Bukkit.addRecipe(LADDERR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_NUGGET,8)).addIngredient(1, Material.GOLDEN_APPLE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,3)).addIngredient(1, Material.BED));
//		//トーチ逆変換（保留）
//		//ShapedRecipe torctR =new ShapedRecipe(new ItemStack(Material.COAL,1));
//		//torctR.shape("XX","XX").setIngredient('X', Material.TORCH);
//		//Bukkit.addRecipe(torctR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STICK,8)).addIngredient(1, Material.PAINTING));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.CLAY_BALL,4)).addIngredient(1, Material.CLAY));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SNOW_BALL,4)).addIngredient(1, Material.SNOW));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.CLAY_BRICK,4)).addIngredient(1, Material.BRICK));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STONE,1)).addIngredient(1, Material.SMOOTH_BRICK));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SAND,4)).addIngredient(1, Material.SANDSTONE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.REDSTONE,1)).addIngredient(1, Material.NOTE_BLOCK));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,1)).addIngredient(1, Material.JUKEBOX));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SULPHUR,5)).addIngredient(1, Material.TNT));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.PUMPKIN,1)).addIngredient(1, Material.JACK_O_LANTERN));
//		ShapedRecipe bowlR =new ShapedRecipe(new ItemStack(Material.WOOD,3));
//		bowlR.shape("XX","XX").setIngredient('X', Material.BOWL);
//		Bukkit.addRecipe(bowlR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GLOWSTONE_DUST,4)).addIngredient(1, Material.GLOWSTONE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STRING,4)).addIngredient(1, Material.WOOL));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STONE,2)).addIngredient(1, Material.STONE_BUTTON));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,3)).addIngredient(1, Material.BUCKET));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,4)).addIngredient(1, Material.WATCH));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,4)).addIngredient(1, Material.COMPASS));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STRING,2)).addIngredient(1, Material.FISHING_ROD,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STRING,3)).addIngredient(1, Material.BOW,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,1)).addIngredient(1, Material.POWERED_RAIL));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,1)).addIngredient(1, Material.DETECTOR_RAIL));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BOW,1)).addIngredient(1, Material.DISPENSER));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,2)).addIngredient(1, Material.SHEARS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COMPASS,1)).addIngredient(1, Material.MAP));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,1)).addIngredient(1, Material.PISTON_BASE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SLIME_BALL,1)).addIngredient(1, Material.PISTON_STICKY_BASE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,3)).addIngredient(1, Material.TRAP_DOOR));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,2)).addIngredient(1, Material.WOOD_PLATE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STONE,2)).addIngredient(1, Material.STONE_PLATE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.REDSTONE,1)).addIngredient(1, Material.REDSTONE_TORCH_ON));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STICK,1)).addIngredient(1, Material.LEVER));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WHEAT,3)).addIngredient(1, Material.CAKE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WHEAT,3)).addIngredient(1, Material.CAKE_BLOCK));
//		ShapedRecipe cookieR =new ShapedRecipe(new ItemStack(Material.WHEAT,1));
//		cookieR.shape("XX","XX").setIngredient('X', Material.COOKIE);
//		Bukkit.addRecipe(cookieR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BOOK,3)).addIngredient(1, Material.BOOKSHELF));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.LEATHER,5)).addIngredient(1, Material.LEATHER_HELMET,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.LEATHER,8)).addIngredient(1, Material.LEATHER_CHESTPLATE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.LEATHER,7)).addIngredient(1, Material.LEATHER_LEGGINGS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.LEATHER,4)).addIngredient(1, Material.LEATHER_BOOTS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,5)).addIngredient(1, Material.IRON_HELMET,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,8)).addIngredient(1, Material.IRON_CHESTPLATE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,7)).addIngredient(1, Material.IRON_LEGGINGS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,4)).addIngredient(1, Material.IRON_BOOTS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,5)).addIngredient(1, Material.GOLD_HELMET,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,8)).addIngredient(1, Material.GOLD_CHESTPLATE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,7)).addIngredient(1, Material.GOLD_LEGGINGS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,4)).addIngredient(1, Material.GOLD_BOOTS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,5)).addIngredient(1, Material.DIAMOND_HELMET,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,8)).addIngredient(1, Material.DIAMOND_CHESTPLATE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,7)).addIngredient(1, Material.DIAMOND_LEGGINGS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,4)).addIngredient(1, Material.DIAMOND_BOOTS,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STICK,5)).addIngredient(1, Material.WOOD_SWORD,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,4)).addIngredient(1, Material.WOOD_PICKAXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,2)).addIngredient(1, Material.WOOD_SPADE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,4)).addIngredient(1, Material.WOOD_AXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,3)).addIngredient(1, Material.WOOD_HOE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE,2)).addIngredient(1, Material.STONE_SWORD,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE,3)).addIngredient(1, Material.STONE_PICKAXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE,1)).addIngredient(1, Material.STONE_SPADE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE,3)).addIngredient(1, Material.STONE_AXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE,2)).addIngredient(1, Material.STONE_HOE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,2)).addIngredient(1, Material.IRON_SWORD,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,3)).addIngredient(1, Material.IRON_PICKAXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,1)).addIngredient(1, Material.IRON_SPADE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,3)).addIngredient(1, Material.IRON_AXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,2)).addIngredient(1, Material.IRON_HOE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,2)).addIngredient(1, Material.GOLD_SWORD,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,3)).addIngredient(1, Material.GOLD_PICKAXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,1)).addIngredient(1, Material.GOLD_SPADE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,3)).addIngredient(1, Material.GOLD_AXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT,2)).addIngredient(1, Material.GOLD_HOE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,2)).addIngredient(1, Material.DIAMOND_SWORD,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,3)).addIngredient(1, Material.DIAMOND_PICKAXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,1)).addIngredient(1, Material.DIAMOND_SPADE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,3)).addIngredient(1, Material.DIAMOND_AXE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,2)).addIngredient(1, Material.DIAMOND_HOE,D));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.MELON,9)).addIngredient(1, Material.MELON_BLOCK));
//		ShapedRecipe railR =new ShapedRecipe(new ItemStack(Material.IRON_INGOT,3));
//		railR.shape("XXX","X X","XXX").setIngredient('X', Material.RAILS);
//		Bukkit.addRecipe(railR);
//		ShapedRecipe windowR =new ShapedRecipe(new ItemStack(Material.GLASS,3));
//		windowR.shape("XXX","X X","XXX").setIngredient('X', Material.THIN_GLASS);
//		Bukkit.addRecipe(windowR);
//		ShapedRecipe ironfensR =new ShapedRecipe(new ItemStack(Material.IRON_INGOT,3));
//		ironfensR.shape("XXX","X X","XXX").setIngredient('X', Material.IRON_FENCE);
//		Bukkit.addRecipe(ironfensR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,4)).addIngredient(1, Material.FENCE_GATE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.CHEST,1)).addIngredient(1, Material.STORAGE_MINECART));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.FURNACE,1)).addIngredient(1, Material.POWERED_MINECART));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.REDSTONE_TORCH_ON,1)).addIngredient(1, Material.DIODE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.MELON,1)).addIngredient(1, Material.MELON_SEEDS));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BONE,1)).addIngredient(1, Material.INK_SACK,15)
//		.addIngredient(1, Material.INK_SACK,15).addIngredient(1, Material.INK_SACK,15));
//		//ShapedRecipe BONER =new ShapedRecipe(new ItemStack(Material.BONE,1));
//		//BONER.shape("XXX").setIngredient('X', Material.INK_SACK,15);
//		//Bukkit.addRecipe(BONER);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.STONE,1)).addIngredient(1, Material.STEP,0));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SANDSTONE,1)).addIngredient(1, Material.STEP,1));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.WOOD,1)).addIngredient(1, Material.STEP,2));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.COBBLESTONE,1)).addIngredient(1, Material.STEP,3));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BRICK,1)).addIngredient(1, Material.STEP,4));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SMOOTH_BRICK,1)).addIngredient(1, Material.STEP,5));
//		ShapedRecipe pumpkinSeedsR =new ShapedRecipe(new ItemStack(Material.PUMPKIN,1));
//		pumpkinSeedsR.shape("XX","XX").setIngredient('X', Material.PUMPKIN_SEEDS);
//		Bukkit.addRecipe(pumpkinSeedsR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.NETHER_BRICK,1)).addIngredient(1, Material.NETHER_FENCE));
//		ShapedRecipe stairsNetherBrickR =new ShapedRecipe(new ItemStack(Material.NETHER_BRICK,3));
//		stairsNetherBrickR.shape("XX").setIngredient('X', Material.NETHER_BRICK_STAIRS);
//		Bukkit.addRecipe(stairsNetherBrickR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BLAZE_POWDER,1)).addIngredient(1, Material.MAGMA_CREAM));
//		ShapedRecipe blazeRodR =new ShapedRecipe(new ItemStack(Material.BLAZE_ROD,1));
//		blazeRodR.shape("XX").setIngredient('X', Material.BLAZE_POWDER);
//		Bukkit.addRecipe(blazeRodR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.SPIDER_EYE,1)).addIngredient(1, Material.FERMENTED_SPIDER_EYE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GOLD_NUGGET,1)).addIngredient(1, Material.SPECKLED_MELON));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GLASS_BOTTLE,1)).addIngredient(1, Material.GLASS_BOTTLE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.IRON_INGOT,7)).addIngredient(1, Material.CAULDRON));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.DIAMOND,2)).addIngredient(1, Material.ENCHANTMENT_TABLE));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BLAZE_ROD,1)).addIngredient(1, Material.BREWING_STAND));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.BLAZE_POWDER,1)).addIngredient(1, Material.EYE_OF_ENDER));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.FLINT,1)).addIngredient(1, Material.GRAVEL));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.GRAVEL,1)).addIngredient(1, Material.FLINT));
//		ShapedRecipe arrowR =new ShapedRecipe(new ItemStack(Material.FLINT,1));
//		arrowR.shape("XX","XX").setIngredient('X', Material.ARROW);
//		Bukkit.addRecipe(arrowR);
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 14)).addIngredient(1, Material.WOOL,1));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 13)).addIngredient(1, Material.WOOL,2));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 12)).addIngredient(1, Material.WOOL,3));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 11)).addIngredient(1, Material.WOOL,4));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 10)).addIngredient(1, Material.WOOL,5));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 9)).addIngredient(1, Material.WOOL,6));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 8)).addIngredient(1, Material.WOOL,7));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 7)).addIngredient(1, Material.WOOL,8));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 6)).addIngredient(1, Material.WOOL,9));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 5)).addIngredient(1, Material.WOOL,10));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 4)).addIngredient(1, Material.WOOL,11));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 3)).addIngredient(1, Material.WOOL,12));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 2)).addIngredient(1, Material.WOOL,13));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 1)).addIngredient(1, Material.WOOL,14));
//		Bukkit.addRecipe(new ShapelessRecipe(new ItemStack(Material.INK_SACK,1, (short) 0)).addIngredient(1, Material.WOOL,15));
	}
}